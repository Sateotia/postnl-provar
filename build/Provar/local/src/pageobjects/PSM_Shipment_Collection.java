package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="PSM_Shipment_Collection"                                
     , summary=""
     , relativeUrl=""
     , connection="CommunityUser"
     )             
public class PSM_Shipment_Collection {

	@LinkType()
	@FindBy(linkText = "Afhaalopdrachten")
	public WebElement afhaalopdrachten;
	@LinkType()
	@FindBy(linkText = "Adresboek openen")
	public WebElement adresboekOpenen;
	@FindBy(className = "addressbook-contact")
	@TextType()
	public WebElement mainContent;
	@LinkType()
	@FindBy(linkText = "Toevoegen")
	public WebElement toevoegen;
	@LinkType()
	@FindBy(xpath = "//*[@id=\"multiButton\"]")
	public WebElement gaVerder;
	@LinkType()
	@FindBy(xpath = "//div[contains(@class, \"optionBlock\")]/ul/li[1]/a")
	public WebElement pakket;
	@ChoiceListType(values = { @ChoiceListValue(value = "Test Sender Belgium 1 - WXXQ - 94312313"), @ChoiceListValue(value = "Test Sender Netherlands - WXXQ - 94312313") })
	@FindBy(xpath = "//*[@id=\"j_id0:j_id1:sendingTopContainer:sender\"]")
	public WebElement afzender11;
	@TextType()
	@FindBy(xpath = "//*[@id=\"j_id0:j_id1:j_id1494:j_id1498:j_id1499:searchContainer\"]//input")
	public WebElement addressBookSearch;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.BackgroundActivity(timeoutSeconds = 60)
	@BooleanType()
	@FindBy(xpath = "//tbody/tr[2]/td[10]/input")
	public WebElement recipientCheckbox;
	@ChoiceListType(values = { @ChoiceListValue(value = "Test Sender Belgium 1 - WXXQ - 94312313"), @ChoiceListValue(value = "Test Sender Germany 1 - WXXQ - 94312314"), @ChoiceListValue(value = "Test Sender Netherlands - WXXQ - 94312313") })
	@FindBy(xpath = "//select[@class=\"markDirty\"][@size=\"1\"][contains(@onchange,\"A4J.AJAX.Submit\")]")
	public WebElement afzenderCollection;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.BackgroundActivity(timeoutSeconds = 10)
	@FindBy(xpath = "//input[@class=\"searchField\"]")
	@TextType()
	public WebElement addressBookSearch1;
			
}
