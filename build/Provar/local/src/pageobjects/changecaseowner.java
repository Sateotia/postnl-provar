package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="changecaseowner"                                
     , summary=""
     , relativeUrl=""
     , connection="DEV40"
     )             
public class changecaseowner {

	@TextType()
	@FindByLabel(label = "Zoeken in bereik")
	public WebElement newOwn;
	@TextType()
	@FindByLabel(label = "Eigenaar")
	public WebElement newOwnName;
	@ButtonType()
	@FindByLabel(label = "Opslaan")
	public WebElement save;
			
}
