package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="Basketpage"                                
               , summary=""
               , page="BasketbuilderApp"
               , namespacePrefix="csbb"
               , object="cscfga__Product_Basket__c"
               , connection="Salesforce"
     )             
public class Basketpage {

	@PageWait.Field(timeoutSeconds = 10)
	@ButtonType()
	@FindBy(xpath = "//button[contains(@class, \"btn-new-basket\")]")
	public WebElement Voegproducttoebtn;
	@PageWait.Field(timeoutSeconds = 10)
	@ButtonType()
	@FindBy(xpath = "//a[contains(text(),\"Partijenpost Binnenland\")]/ancestor::div[@class=\"summary-row\"]//button[@class=\"btn btn-edit ng-scope\"]")
	public WebElement EditconfigPartijenpostbinnneland;
	@PageWait.Field(timeoutSeconds = 10)
	@FindBy(xpath = "//a[contains(text(),\"Zakelijke Pakketten\")]/ancestor::div[@class=\"summary-row\"]//button[@class=\"btn btn-edit ng-scope\"]")
	@TextType()
	public WebElement EditconfigZakelijkepakketten;
	
	}
	

