package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="BP_LabelGenerationPage"                                
     , summary=""
     , relativeUrl=""
     , connection="CommunityUser"
     )             
public class BP_LabelGenerationPage {

	@FindBy(linkText = "Printen")
	@LinkType()
	public WebElement printen1;
			
}
