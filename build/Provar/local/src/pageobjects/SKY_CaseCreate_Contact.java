package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="SKY_ Case Create_ Contact"                                
               , summary=""
               , page="SKY_CaseCreate_Contact"
               , namespacePrefix=""
               , object="Contact"
               , connection="SkyTest"
     )             
public class SKY_CaseCreate_Contact {

	@LinkType()
	@FindBy(linkText = "ZENDING NIET ONTVANGEN")
	public WebElement zENDINGNIETONTVANGEN;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.BackgroundActivity(timeoutSeconds = 60)
	@ButtonType()
	@FindByLabel(label = "Case aanmaken")
	public WebElement caseAanmaken;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.BackgroundActivity(timeoutSeconds = 60)
	@ButtonType()
	@FindBy(xpath = "//div[contains(@class, \"slds-grid\")]//button[2]")
	public WebElement caseAanmaken1;
	@TextType()
	@FindBy(xpath = "//div[4]/ul/li[2]/div/div[2]/nav/ol")
	public WebElement mailadministratiefactuurvragen;
	@FindBy(linkText = "FACTUURVRAGEN PAKKET")
	@TextType()
	public WebElement factuurvragen_pakket;
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"slds-lookup__menu\")]/div[3]/ul/li[1]/div/div[2]/nav/ol")
	public WebElement DistributieBeschadigd;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@FindBy(css = "div.slds-show li.slds-box ol.slds-breadcrumb.slds-list--horizontal")
	@TextType()
	public WebElement onterecht_in_brievenbus;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@ButtonType()
	@FindByLabel(label = "Case opslaan")
	public WebElement caseOpslaan;
	@PageWait.Field(field = "CaseCategorySearch", timeoutSeconds = 10)
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"slds-form-element__control\")]/input[1]")
	public WebElement CaseCategorySearch;
	@ChoiceListType(values = { @ChoiceListValue(value = "Handtekening"), @ChoiceListValue(value = "Spoed"), @ChoiceListValue(value = "Standaard"), @ChoiceListValue(value = "Verzekerd"), @ChoiceListValue(value = "Betalen_bij ontvangst", title = "Betalen bij ontvangst"), @ChoiceListValue(value = "Brief_aangetekend", title = "Brief aangetekend"), @ChoiceListValue(value = "Aangetekend"), @ChoiceListValue(value = "Afhaalservice"), @ChoiceListValue(value = "Brief_verzekerd", title = "Brief verzekerd"), @ChoiceListValue(value = "Brievenbuspakje") })
	@FindBy(xpath = "//*[@name=\"productSelect\"]")
	public WebElement ProductSelectAlternative;
	@ChoiceListType(values = { @ChoiceListValue(value = "Handtekening"), @ChoiceListValue(value = "Spoed"), @ChoiceListValue(value = "Standaard"), @ChoiceListValue(value = "Verzekerd"), @ChoiceListValue(value = "Betalen_bij ontvangst", title = "Betalen bij ontvangst"), @ChoiceListValue(value = "Brief_aangetekend", title = "Brief aangetekend"), @ChoiceListValue(value = "Aangetekend"), @ChoiceListValue(value = "Afhaalservice"), @ChoiceListValue(value = "Brief_verzekerd", title = "Brief verzekerd"), @ChoiceListValue(value = "Brievenbuspakje"), @ChoiceListValue(value = "Verkiezingspost"), @ChoiceListValue(value = "Briefpostzending"), @ChoiceListValue(value = "Briefpostzending_Internationaal", title = "Briefpostzending Internationaal"), @ChoiceListValue(value = "Buspakje"), @ChoiceListValue(value = "Diagnostische_Zendingen", title = "Diagnostische Zendingen"), @ChoiceListValue(value = "Rouwkaart") })
	@FindBy(xpath = "//*[@name=\"productSelect\"]")
	public WebElement ProductSelectExtended;
	@ChoiceListType(values = { @ChoiceListValue(value = "Sender", title = "Afzender"), @ChoiceListValue(value = "Receiver", title = "Geadresseerde"), @ChoiceListValue(value = "Third Party", title = "Derde Partij") })
	@FindBy(xpath = "//*[@name=\"selectReclamantName\"]")
	public WebElement WieIsDeReclamant;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@BooleanType()
	@FindBy(xpath = "//div[contains(@class, \"slds-form-element\")]/div[2]/div/div/div/input")
	public WebElement OlderThan3Months;
	@ChoiceListType(values = { @ChoiceListValue(value = "Receiver", title = "Geadresseerde"), @ChoiceListValue(value = "Sender", title = "Afzender"), @ChoiceListValue(value = "Third Party", title = "Derde Partij") })
	@FindBy(xpath = "//*[@id=\"397:0\"]")
	public WebElement ReclamantBarcode;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(field = "BarcodeCaseCategorySearch", timeoutSeconds = 10)
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"slds-show\")]//input")
	public WebElement BarcodeCaseCategorySearch;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.BackgroundActivity(timeoutSeconds = 60)
	@FindBy(xpath = "//input[@placeholder='Zoek op Klantsignaal Onderwerp/Inhoud of op Barcode...']")
	@TextType()
	public WebElement Pakket_Webform;
	
}
