package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="SKY_ Convert To Case"                                
               , summary=""
               , page="SKY_ConvertToCase"
               , namespacePrefix=""
               , object="Case"
               , connection="SkyTest"
     )             
public class SKY_ConvertToCase {

	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"slds-form-element__control\")]/input[1]")
	public WebElement Case_Category_Search;
	@TextType()
	@FindBy(css = "div.slds-lookup__menu.slds-show li.slds-box ol.slds-breadcrumb.slds-list--horizontal")
	public WebElement Category;
	@ButtonType()
	@FindBy(xpath = "//div[contains(@class, \"slds-grid\")]//button[2]")
	public WebElement case_Aanmaken1;
	
}
