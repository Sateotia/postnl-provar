package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="SKY_k Container"                                
               , summary=""
               , page="SKY_KContainer"
               , namespacePrefix=""
               , object="Case"
               , connection="SkyTest"
     )             
public class SKY_kContainer {

	@LinkType()
	@FindBy(linkText = "23-11-2017")
	public WebElement datum;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.BackgroundActivity(timeoutSeconds = 60)
	@ButtonType()
	@FindByLabel(label = "Volgende")
	public WebElement volgende;
	@BooleanType()
	@FindByLabel(label = "Ja")
	public WebElement ja;
	@BooleanType()
	@FindByLabel(label = "Afzender")
	public WebElement afzender;
	@TextType()
	@FindByLabel(label = "Postcode")
	public WebElement postcode;
	@TextType()
	@FindByLabel(label = "Plaats")
	public WebElement plaats;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.BackgroundActivity(timeoutSeconds = 60)
	@BooleanType()
	@FindByLabel(label = "Beschadigd")
	public WebElement beschadigd;
	@BooleanType()
	@FindByLabel(label = "Met excuus")
	public WebElement metExcuus;
	@TextType()
	@FindBy(xpath = "//tbody/tr[3]/td/span/span/font/b")
	public WebElement Display_Promise;
	@LinkType()
	@FindBy(css = "span.dateFormat a")
	public WebElement datumToday;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"requiredInput\")]//textarea")
	public WebElement HoeVerpakt;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"requiredInput\")]//textarea")
	public WebElement Wat_Zichtbaar;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.BackgroundActivity(timeoutSeconds = 60)
	@BooleanType()
	@FindBy(xpath = "//tr[3]/td[2]/div/div[3]/input")
	public WebElement LeegOfBeschadigd;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.BackgroundActivity(timeoutSeconds = 60)
	@BooleanType()
	@FindBy(xpath = "//tr[8]/td[2]/div/div[3]/label")
	public WebElement BeschadigdZonderExcuus;
	@BooleanType()
	@FindBy(xpath = "//tr[6]/td[2]/div/div[3]/input")
	public WebElement SchadeZichtbaarNee;
	@ChoiceListType(values = { @ChoiceListValue(value = "Choice_Feedback_Medium.Keuze1", title = "Telefoon"), @ChoiceListValue(value = "Choice_Feedback_Medium.Keuze2", title = "E-mail"), @ChoiceListValue(value = "Choice_Feedback_Medium.Keuze3", title = "Niet aangegeven"), @ChoiceListValue(value = "Choice_CaseVariableFeedbackMedium") })
	@FindBy(xpath = "//div[contains(@class, \"requiredInput\")]//select")
	public WebElement TerugkoppelMedium;
	@TextType()
	@FindBy(xpath = "//tr[4]/td[2]/div/input")
	public WebElement huisnummerOfPostbusnummer;
	
}
