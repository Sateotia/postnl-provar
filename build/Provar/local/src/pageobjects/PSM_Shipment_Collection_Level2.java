package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="PSM_Shipment_Collection_Level2"                                
     , summary=""
     , relativeUrl=""
     , connection="CommunityUser"
     )             
public class PSM_Shipment_Collection_Level2 {

	@LinkType()
	@FindBy(linkText = "Afhaalservice Basis")
	public WebElement afhaalserviceBasis;
	@LinkType()
	@FindBy(linkText = "Volgende")
	public WebElement volgende;
	@LinkType()
	@FindBy(linkText = "Gegevens opslaan en verder")
	public WebElement gegevensOpslaanEnVerder;
	@TextType()
	@FindByLabel(label = "Afhaaldatum*")
	public WebElement afhaaldatum_;
	@LinkType()
	@FindBy(css = "#saveShipment")
	public WebElement gegevensOpslaanEnVerder1;
	@LinkType()
	@FindBy(xpath = "//*[@id=\"productWizard\"]/div[2]/ul/li[2]/a")
	public WebElement afhaalservicePlus;
	@LinkType()
	@FindBy(xpath = "//*[@id=\"manualOpenAddressBook\"]")
	public WebElement collectionAddressContainer;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@BooleanType()
	@FindBy(xpath = "//*[@id=\"j_id0:j_id1:j_id2221:j_id2225:j_id2226:mainContent\"]/table/tbody/tr[2]/td[10]/input")
	public WebElement recipientCheckbox;
	@PageWait.BackgroundActivity(timeoutSeconds = 60)
	@LinkType()
	@FindBy(linkText = "Toevoegen")
	public WebElement toevoegen;
	@PageWait.BackgroundActivity(timeoutSeconds = 60)
	@FindBy(xpath = "//*[@id=\"j_id0:j_id1:j_id2248:j_id2252:j_id2253:mainContent\"]/table/tbody/tr[2]/td[10]/input")
	@BooleanType()
	public WebElement RecipientsCheckbox;
			
}
