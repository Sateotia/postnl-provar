package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="BP_LabelGenerationPage2"                                
     , summary=""
     , relativeUrl=""
     , connection="CommunityUser"
     )             
public class BP_LabelGenerationPage2 {

	@LinkType()
	@FindBy(linkText = "Printen")
	public WebElement printen1;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(field = "actie", timeoutSeconds = 10)
	@LinkType()
	@FindBy(linkText = "Download")
	public WebElement actie;
			
}
