package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="SKY_k Container GS"                                
               , summary=""
               , page="SKY_KContainer"
               , namespacePrefix=""
               , object="Case"
               , connection="Salesforce"
     )             
public class SKY_kContainerGS {

	@TextType()
	@FindBy(id = "scriptPage:j_id37:i:f:pb:d:Text_OmschrijvingGedrag.input")
	public WebElement Text_OmschrijvingGedrag_input;
	@LinkType()
	@FindBy(linkText = "23-3-2018")
	public WebElement datum;
	@ButtonType()
	@FindByLabel(label = "Volgende")
	public WebElement volgende;
	@BooleanType()
	@FindByLabel(label = "Ja")
	public WebElement ja;
	@LinkType()
	@FindBy(linkText = "26-3-2018")
	public WebElement datum1;
	@LinkType()
	@FindBy(xpath = "//span[contains(@class, \"dateFormat\")]//a")
	public WebElement datum2;
	@ChoiceListType(values = { @ChoiceListValue(value = "X0", title = "00"),
			@ChoiceListValue(value = "X08", title = "08"), @ChoiceListValue(value = "X09", title = "09"),
			@ChoiceListValue(value = "X10", title = "10"), @ChoiceListValue(value = "X11", title = "11"),
			@ChoiceListValue(value = "X12", title = "12"), @ChoiceListValue(value = "X13", title = "13"),
			@ChoiceListValue(value = "X14", title = "14"), @ChoiceListValue(value = "X15", title = "15"),
			@ChoiceListValue(value = "X16", title = "16"), @ChoiceListValue(value = "X17", title = "17"),
			@ChoiceListValue(value = "X18", title = "18"), @ChoiceListValue(value = "X19", title = "19"),
			@ChoiceListValue(value = "X20", title = "20") })
	@FindByLabel(label = "Uren")
	public WebElement uren;
	@ChoiceListType(values = { @ChoiceListValue(value = "X0", title = "00"),
			@ChoiceListValue(value = "X08", title = "08"), @ChoiceListValue(value = "X09", title = "09"),
			@ChoiceListValue(value = "X10", title = "10"), @ChoiceListValue(value = "X11", title = "11"),
			@ChoiceListValue(value = "X12", title = "12"), @ChoiceListValue(value = "X13", title = "13"),
			@ChoiceListValue(value = "X14", title = "14"), @ChoiceListValue(value = "X15", title = "15"),
			@ChoiceListValue(value = "X16", title = "16"), @ChoiceListValue(value = "X17", title = "17"),
			@ChoiceListValue(value = "X18", title = "18"), @ChoiceListValue(value = "X19", title = "19"),
			@ChoiceListValue(value = "X20", title = "20") })
	@FindByLabel(label = "Uren")
	public WebElement uren1;
	
}
