package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="Scherm1_Productconfig_Pakketten"                                
     , connection="UAT_Wilbert"
     )             
public class Scherm1_Productconfig_Pakketten {
@FindBy(xpath = "//*[contains(@src,'cscfga__ConfigureProduct')]")
		public FrameProdConfigPakketten frame_Productconfig1;

	@PageFrame()
	public static class FrameProdConfigPakketten {

		@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindBy(xpath = "//input[@name=\"Num_Colli_0\"]")
		public WebElement AantalColli;
		
		@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindBy(xpath = "//input[@name=\"Num_Shipments_0\"]")
		public WebElement aantalZendingen;
		
		@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindBy(xpath = "//input[@name=\"Num_Weight_0\"]")
		public WebElement GemGewichtCollo;
		
		@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindBy(xpath = "//input[@name=\"Num_Density_0\"]")
		public WebElement DichtheidPerCollo;
		
		@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindBy(xpath = "//select[@name=\"List_TariffLine_0\")]")
		public WebElement tarriffLine;
		
		@ButtonType()
		@FindBy(xpath = "//div[@class=\"pbHeader\"]//button[text()=\"Next\"]")
		public WebElement Nextbutton;
		
		@ChoiceListType(values = { @ChoiceListValue(value = "Collo, Kilo"), @ChoiceListValue(value = "Zending, Collo, Kilo") })
		@FindBy(xpath = "//*[@id=\"Invoer:Volume:List_TariffLine_0\"]")
		public WebElement tarifeerPer;
	}

	
}
