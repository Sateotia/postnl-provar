package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="Productconfig2"                                
     , summary=""
     , relativeUrl=""
     , connection="Salesforce"
     )             
public class Productconfig2 {
			@FindBy(xpath = "//*[contains(@src,'cscfga__ConfigureProduct')]")
		public Frame frame;
		
		@PageFrame()
		public static class Frame {
			@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
			@PageWait.Field(timeoutSeconds = 10)
			@TextType()
			@FindBy(xpath = "//select[contains(@name,'Products_0:List_TariffLine_0')]")
			public WebElement tarriffLine;
			
			@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
			@PageWait.Field(timeoutSeconds = 10)
			@LinkType()
			@FindBy(xpath = "//label[text()=\"Product\"]/ancestor::td/following-sibling::td[1]//a")
			public WebElement productLink;
			
			@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
			@TextType()
			@PageWait.Field(timeoutSeconds = 10)
			@FindBy(xpath = "//div[contains(text(),\"Service Add On Price Item Name\")]/ancestor::ul/ancestor::div//input")
			public WebElement Product_Search;
			
			@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
			@PageWait.Field(timeoutSeconds = 10)
			@LinkType()
			@FindBy(xpath = "//label[text()='Servicekader & Soort']/ancestor::td/following-sibling::td[1]//a")
			public WebElement ServicekaderSoortLink;
			
			@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
			@PageWait.Field(timeoutSeconds = 10)
			@TextType()
			@FindBy(xpath = "//div[contains(text(),\"Service Level\")]/ancestor::ul/ancestor::div//input")
			public WebElement Servicekader_Search;
			
			@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
			@PageWait.Field(timeoutSeconds = 10)
			@LinkType()
			@FindBy(xpath = "//label[text()='Aantal range']/ancestor::td/following-sibling::td[1]//a")
			public WebElement AantalRangeLink;
			
			@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
			@PageWait.Field(timeoutSeconds = 10)
			@TextType()
			@FindBy(xpath = "//*[@id=\"Product:General_Information:TI_Comment_0\"]")
			public WebElement toelichtingAM;
			
			@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
			@PageWait.Field(timeoutSeconds = 10)
			@ButtonType()
			@FindBy(xpath = "//div[@class=\"pbHeader\"]//button[text()=\"Continue\"]")
			public WebElement continue_button;

			@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
			@PageWait.Field(timeoutSeconds = 10)
			@TextType()
			@FindBy(xpath = "//div[contains(@title, \"Brief Klein\")]")
			public WebElement SelectProductFromList;
			
			@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
			@PageWait.Field(timeoutSeconds = 10)
			@TextType()
			@FindBy(xpath = "//div[contains(@title, \"Basic\")]")
			public WebElement SelectServiceKaderFromList;

			@PageWaitAfter.Field(timeoutSeconds = 10)
			@PageWait.Field(timeoutSeconds = 10)
			@TextType()
			@FindBy(xpath = "//tr[(td[1]='250') and (td[2]='2.500')] ")
			public WebElement Click_AantalRange;
		
			@PageTable(row = Columns.class)
			@FindBy(xpath = "//div/ul/li[contains(@class,'select2-results-dept-0')]")
			public List<Columns> list;
 
			@PageRow()
			public static class Columns {
 
			@LinkType()
			@FindBy(xpath = ".//div[@class='rTableCell'][1]")
			public WebElement Minimum;
 
			@LinkType()
			@FindBy(xpath = ".//div[@class='rTableCell'][2]")
			public WebElement Maximum;
}
		
		}

		
		
}
		
			

			

