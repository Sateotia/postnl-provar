package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title = "Mail_Product_Select", page="BasketbuilderApp"
               , namespacePrefix="csbb"
               , object="cscfga__Product_Basket__c"
               , connection="Salesforce"
     )             
public class Mail_Product_Select {

	@PageFrame()
	public static class Mail_Product_Config {
	}

	@LinkType()
	@FindBy(xpath = "//ul[contains(@class, \"jstree-container-ul\")]//ul/li//*[contains(text(),\"Partijenpost Binnenland\")]")
	public WebElement partijenpostBinnenland;
	@FindBy(xpath = "//div[contains(@class, \"modal-footer\")]//button[contains(@class, \"icon-add\")]")
	@ButtonType()
	public WebElement AddtoBasket;
	@ButtonType()
	@FindBy(xpath = "//div[contains(@class, \"table-cell\")]//button[contains(@class, \"btn-edit\")]")
	public WebElement EditBasket;
	
}
