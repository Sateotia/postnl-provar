package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="TT_Sent_Shipments"                                
     , summary = "Track & Trace page", connection="CommunityUser"
     )             
public class TT_Sent_Shipments {

	@LinkType()
	@FindBy(xpath = "//div[contains(@class, \"clear\")]/a[1]")
	public WebElement zoeken;
	@ChoiceListType(values = { @ChoiceListValue(value = "Today", title = "Vandaag"),
			@ChoiceListValue(value = "Yesterday", title = "Gisteren"),
			@ChoiceListValue(value = "LastWeek", title = "Laatste week"),
			@ChoiceListValue(value = "LastMonth", title = "Laatste maand") })
	@FindBy(xpath = "//select[@class=\"form-control\"][@data-filtername=\"filterPeriod\"]")
	public WebElement filterPeriod;
	@TextType()
	@FindBy(xpath = "//tr[2]//td[4]")
	public WebElement shipmentTable;
	@LinkType()
	@FindBy(xpath = "//ul[contains(@class, \"floatMenu\")]/li[1]/a")
	public WebElement overzichtPrinten;
			
}
