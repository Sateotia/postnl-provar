package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="Productconfig1"                                
     , connection="Salesforce"
     )             
public class Productconfig1 {
			@FindBy(xpath = "//*[contains(@src,'cscfga__ConfigureProduct')]")
		public Frame2 frame_Productconfig1;

@PageFrame()
public static class Frame2 {
		@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindBy(xpath = "//input[@name=\"Num_Annual_Turnover_0\"]")
		public WebElement jaaromzet;
		
		@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
		@PageWait.Field(timeoutSeconds = 10)
		@TextType()
		@FindBy(xpath = "//input[@name=\"Num_Volume_0\"]")
		public WebElement jaarvolume;
		
		@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
		@PageWait.Field(timeoutSeconds = 10)
		@ButtonType()
		@FindBy(xpath = "//tr//button[text()=\"New Products\"]")
		public WebElement newProducts;
		
		@ButtonType()
		@FindBy(xpath = "//div[@class=\"pbHeader\"]//button[text()=\"Finish\"]")
		public WebElement finish;
	}
			
}

