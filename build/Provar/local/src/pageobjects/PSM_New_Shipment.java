package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="PSM_New_Shipment"                                
     , summary=""
     , relativeUrl=""
     , connection="CommunityUser"
     )             
public class PSM_New_Shipment {

	@LinkType()
	@FindBy(linkText = "Adresboek openen")
	public WebElement adresboekOpenen;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@BooleanType()
	@FindBy(xpath = "//tbody/tr[2]/td[10]/input")
	public WebElement recipientCheckbox;
	@LinkType()
	@FindBy(linkText = "Toevoegen")
	public WebElement toevoegen;
	@LinkType()
	@FindBy(linkText = "Ga verder\n                                                                    ")
	public WebElement gaVerder;
	@LinkType()
	@FindBy(xpath = "//*[@id=\"multiButton\"]")
	public WebElement gaVerder1;
	@LinkType()
	@FindBy(xpath = "//*[@id=\"productWizard\"]/div[1]/ul/li[1]/a")
	public WebElement pakket;
	@TextType()
	@FindBy(xpath = "//*[@id=\"productWizard\"]/div[2]/ul/li[1]/a")
	public WebElement adresOntvanger2;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(field = "standaard", timeoutSeconds = 10)
	@TextType()
	@FindBy(xpath = "//*[@id=\"productWizard\"]/div[3]/ul/li[1]/a")
	public WebElement standaard;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(field = "handtekeningVoorOntvangst", timeoutSeconds = 60)
	@TextType()
	@FindBy(xpath = "//*[@id=\"productWizard\"]/div[4]/ul/li[1]/a")
	public WebElement handtekeningVoorOntvangst;
	@LinkType()
	@FindBy(linkText = "Volgende")
	public WebElement volgende;
	@LinkType()
	@FindBy(linkText = "Gegevens opslaan en verder")
	public WebElement gegevensOpslaanEnVerder;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(field = "printenEnBevestigen", timeoutSeconds = 10)
	@LinkType()
	@FindBy(linkText = "Printen en bevestigen")
	public WebElement printenEnBevestigen;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(field = "printen", timeoutSeconds = 10)
	@LinkType()
	@FindBy(linkText = "Printen")
	public WebElement printen;
	@LinkType()
	@FindBy(linkText = "Download")
	public WebElement actie;
	@ChoiceListType(values = { @ChoiceListValue(value = "Test Sender Luxembourg 1 - WXXQ - 94312313"), @ChoiceListValue(value = "Test Sender Netherlands - WXXQ - 94312313") })
	@FindByLabel(label = "Afzender")
	public WebElement afzender;
	@ChoiceListType(values = { @ChoiceListValue(value = "Test Sender Netherlands - WXXQ - 94312313") })
	@FindBy(xpath = "//*[@id=\"j_id0:j_id1:sendingTopContainer:sender\"]")
	public WebElement afzender1;
	@TextType()
	@FindBy(xpath = "//tbody/tr[3]/td[8]/div")
	public WebElement mainContent;
	@ChoiceListType(values = { @ChoiceListValue(value = "Test Sender Belgium 1 - WXXQ - 94312313"), @ChoiceListValue(value = "Test Sender Netherlands - WXXQ - 94312313") })
	@FindBy(xpath = "//select[@class=\"markDirty\"][contains(@onchange,\"A4J.AJAX.Submit\")]")
	public WebElement afzender11;
	@TextType()
	@FindBy(xpath = "//tbody/tr[3]/td[8]/div")
	public WebElement mainContent1;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.BackgroundActivity(timeoutSeconds = 60)
	@TextType()
	@FindBy(xpath = "//input[@class=\"searchField\"]")
	public WebElement addressBookSearch;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.BackgroundActivity(timeoutSeconds = 60)
	@LinkType()
	@FindBy(xpath = "//*[@id=\"productWizard\"]/div[2]/ul/li[3]/a")
	public WebElement pastDoorBrievenbus;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(field = "brievenbuspakjePTP", timeoutSeconds = 10)
	@LinkType()
	@FindBy(xpath = "//*[@id=\"productWizard\"]/div[3]/ul/li[2]/a")
	public WebElement brievenbuspakjePTP;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(field = "BrievenbuspakjeOnline", timeoutSeconds = 10)
	@TextType()
	@FindBy(xpath = "//*[@id=\"productWizard\"]/div[3]/ul/li[1]/a/span/div")
	public WebElement BrievenbuspakjeOnline;
	@TextType()
	@FindBy(css = "div.icon.icon-Home")
	public WebElement AdresOntvanger;
	@TextType()
	@FindBy(xpath = "//*[@id=\"productWizard\"]/div[3]/ul/li[1]/a/span/div")
	public WebElement Metaanvullendediensten;
	@ChoiceListType(values = { @ChoiceListValue(value = "Geschenk"), @ChoiceListValue(value = "Documenten"), @ChoiceListValue(value = "Handelsgoederen"), @ChoiceListValue(value = "Handelsmonster"), @ChoiceListValue(value = "Retourgoederen") })
	@FindBy(xpath = "//*[@id=\"customsDeclarationContainer\"]/div[1]/div/select")
	public WebElement pakkettype;
	@TextType()
	@FindBy(xpath = "//span[contains(@class, \"customs-item-row\")]/div[1]/div/input")
	public WebElement artikelbeschrijving;
	@TextType()
	@FindBy(xpath = "//span[contains(@class, \"customs-item-row\")]/div[2]/div/input")
	public WebElement aantal;
	@TextType()
	@FindBy(xpath = "//span[contains(@class, \"customs-item-row\")]/div[3]/div/input")
	public WebElement gewichtGram;
	@TextType()
	@FindBy(xpath = "//span[contains(@class, \"customs-item-row\")]/div[4]/div/input")
	public WebElement bedrag;
	@LinkType()
	@FindBy(xpath = "//ul[contains(@class, \"selectTiles\")]/li[2]/a")
	public WebElement directParcel;
	@TextType()
	@FindBy(xpath = "//*[@id=\"orderQuestionsContainer\"]/div[3]/div/input")
	public WebElement uwOrdernummer;
	@ChoiceListType(values = { @ChoiceListValue(value = "Buitenlands paspoort") })
	@FindBy(xpath = "//*[@id=\"orderQuestionsContainer\"]/div[4]/div/select")
	public WebElement legimitatieSoort;
	@TextType()
	@FindBy(xpath = "//*[@id=\"orderQuestionsContainer\"]/div[6]/div/input")
	public WebElement legitimatieNummer;
	@TextType()
	@FindBy(xpath = "//*[@id=\"singlePackage\"]/div/div/input")
	public WebElement mobielTelefoonnummerCN;
	@TextType()
	@FindBy(xpath = "//*[@id=\"customsDeclarationContainer\"]/div[1]/div/input")
	public WebElement importheffing;
	@TextType()
	@FindBy(xpath = "//span[contains(@class, \"customs-item-row\")]/div[1]/div/input")
	public WebElement EAN_Code_;
	@TextType()
	@FindBy(xpath = "//span[contains(@class, \"customs-item-row\")]/div[2]/div/input")
	public WebElement URL;
	@TextType()
	@FindBy(xpath = "//span[contains(@class, \"customs-item-row\")]/div[3]/div/input")
	public WebElement Artikelbeschrijving;
	@TextType()
	@FindBy(xpath = "//span[contains(@class, \"customs-item-row\")]/div[4]/div/input")
	public WebElement Aantaldirectparcel;
	@TextType()
	@FindBy(xpath = "//span[contains(@class, \"customs-item-row\")]/div[5]/div/input")
	public WebElement gewichtGram1;
	@TextType()
	@FindBy(xpath = "//span[contains(@class, \"customs-item-row\")]/div[6]/div/input")
	public WebElement bedrag1;
	@TextType()
	@FindBy(xpath = "//span[contains(@class, \"customs-item-row\")]/div[7]/div/input")
	public WebElement goederencodeHSCode;
	@TextType()
	@FindBy(css = "div.icon.icon-ExtraHome.icon-ExtraHome-w")
	public WebElement Extra_home;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(field = "__mans", timeoutSeconds = 10)
	@TextType()
	@FindBy(xpath = "//*[@id=\"productWizard\"]/div[2]/ul/li[2]/a/span/div")
	public WebElement __mans;
	@TextType()
	@FindBy(xpath = "//*[@id=\"productWizard\"]/div[3]/ul/li[1]/a/span/div")
	public WebElement Drempelservice;
	@LinkType()
	@FindBy(xpath = "//ul[contains(@class, \"selectTiles\")]/li[2]/a")
	public WebElement Extra_home1;
	@TextType()
	@FindBy(xpath = "//*[@id=\"orderQuestionsContainer\"]/div[1]/div[1]/input")
	public WebElement uwReferentie;
	@TextType()
	@FindBy(xpath = "//*[@id=\"singlePackage\"]/div[1]/div/input")
	public WebElement gewichtGram2;
	@TextType()
	@FindBy(xpath = "//*[@id=\"singlePackage\"]/div[4]/div/input")
	public WebElement Volume_E_H;
	@TextType()
	@FindBy(xpath = "//*[@id=\"singlePackage\"]/div[5]/div[1]/input")
	public WebElement Omschrijving_E_H;
	@TextType()
	@FindBy(xpath = "//*[@id=\"singlePackage\"]/div[2]/div[1]/input")
	public WebElement mobiel_nummer_E_H;
	@TextType()
	@FindBy(xpath = "//*[@id=\"singlePackage\"]/div[3]/div/input")
	public WebElement email_E_H;
			
}
