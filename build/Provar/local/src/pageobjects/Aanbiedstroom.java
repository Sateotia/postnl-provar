package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( connection="SkyTest"
     )             
public class Aanbiedstroom {

	@PageWait.Field(field = "zakelijkeKlant", timeoutSeconds = 10)
	@BooleanType()
	@FindByLabel(label = "Zakelijke klant")
	public WebElement zakelijkeKlant;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@ButtonType()
	@FindByLabel(label = "Volgende")
	public WebElement volgende;
	@BooleanType()
	@FindByLabel(label = "Ja")
	public WebElement ja;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@TextType()
	@FindByLabel(label = "KVK Nummer")
	public WebElement kVKNummer;
	@FindBy(xpath = "//select")
	@ChoiceListType(values = {
			@ChoiceListValue(value = "DChoice_KVKCollectionOutput.Keuze1", title = "N.M. de Haan - Theo Uden Masmanstraat 2 - AMERSFOORT") })
	public WebElement Dropdown_KVKCollectionOutput_input2;
	@TextType()
	@FindBy(xpath = "//tbody/tr[5]/td/span/div[2]")
	public WebElement Display_KVKRecordOutputKVKNumber1;
	@TextType()
	@FindByLabel(label = "Factuur Email")
	public WebElement factuurEmail;
	@TextType()
	@FindByLabel(label = "IBAN Rekeningnummer")
	public WebElement iBANRekeningnummer;
	@TextType()
	@FindByLabel(label = "Voornaam")
	public WebElement voornaam;
	@ChoiceListType(values = { @ChoiceListValue(value = "Choice_NewTitle"), @ChoiceListValue(value = "PChoice_Title.Keuze1", title = "dr."), @ChoiceListValue(value = "PChoice_Title.Keuze2", title = "drs."), @ChoiceListValue(value = "PChoice_Title.Keuze3", title = "prof."), @ChoiceListValue(value = "PChoice_Title.Keuze4", title = "ing."), @ChoiceListValue(value = "PChoice_Title.Keuze5", title = "ir."), @ChoiceListValue(value = "PChoice_Title.Keuze6", title = "mr."), @ChoiceListValue(value = "PChoice_Title.Keuze7", title = "dr. drs."), @ChoiceListValue(value = "PChoice_Title.Keuze8", title = "dr. ir."), @ChoiceListValue(value = "PChoice_Title.Keuze9", title = "drs. ing."), @ChoiceListValue(value = "PChoice_Title.Keuze10", title = "drs. ir."), @ChoiceListValue(value = "PChoice_Title.Keuze11", title = "prof. dr."), @ChoiceListValue(value = "PChoice_Title.Keuze12", title = "prof. dr. ir."), @ChoiceListValue(value = "PChoice_Title.Keuze13", title = "prof. mr."), @ChoiceListValue(value = "PChoice_Title.Keuze14", title = "mr. dr."), @ChoiceListValue(value = "PChoice_Title.Keuze15", title = "mr. drs."), @ChoiceListValue(value = "PChoice_Title.Keuze16", title = "mr. drs. ing."), @ChoiceListValue(value = "PChoice_Title.Keuze17", title = "mr. ing."), @ChoiceListValue(value = "PChoice_Title.Keuze18", title = "mr. ir.") })
	@FindByLabel(label = "Titel")
	public WebElement titel;
	@FindBy(xpath = "//tr[11]/td[2]/input[1]")
	@BooleanType()
	public WebElement Check_PaymentAgreement_input;
	@FindBy(xpath = "//tr[28]//input")
	@TextType()
	public WebElement telefoon;
	@TextType()
	@FindByLabel(label = "E-mail")
	public WebElement eMail;
	@ChoiceListType(values = {
			@ChoiceListValue(value = "Choice_ContactFunctionNewAccount", title = "-- Selecteer een waarde --"), @ChoiceListValue(value = "PChoice_ContactFunction.Keuze1", title = "Bedrijfsleider"), @ChoiceListValue(value = "PChoice_ContactFunction.Keuze2", title = "Bestuurder"), @ChoiceListValue(value = "PChoice_ContactFunction.Keuze3", title = "Directeur"), @ChoiceListValue(value = "PChoice_ContactFunction.Keuze4", title = "Eigenaar"), @ChoiceListValue(value = "PChoice_ContactFunction.Keuze5", title = "Hoofd/Teamleider"), @ChoiceListValue(value = "PChoice_ContactFunction.Keuze6", title = "Manager"), @ChoiceListValue(value = "PChoice_ContactFunction.Keuze7", title = "Medewerker"), @ChoiceListValue(value = "PChoice_ContactFunction.Keuze8", title = "Onbekend"), @ChoiceListValue(value = "PChoice_ContactFunction.Keuze9", title = "Secr./Telef."), @ChoiceListValue(value = "PChoice_ContactFunction.Keuze10", title = "Staf") })
	@FindByLabel(label = "Functie")
	public WebElement functie;
	@ChoiceListType(values = {
			@ChoiceListValue(value = "Choice_ContactDepartmentNewAccount", title = "-- Selecteer een waarde --"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze1", title = "Algemene zaken"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze2", title = "Automatisering/IT"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze3", title = "Directie"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze4", title = "E-commerce"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze5", title = "Facilitaire zaken"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze6", title = "Financiën"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze7", title = "Fundraising"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze8", title = "Inkoop"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze9", title = "Klantenservice"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze10", title = "Kwaliteitsbeheer/DZO"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze11", title = "Logistiek"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze12", title = "Marketing/Communicatie"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze13", title = "Onbekend"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze14", title = "Personeelszaken"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze15", title = "Postkamer"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze16", title = "Productie"), @ChoiceListValue(value = "PChoice_ContactAfdeling.Keuze17", title = "Verkoop") })
	@FindByLabel(label = "Afdeling")
	public WebElement afdeling;
	@ChoiceListType(values = {
			@ChoiceListValue(value = "Choice_SelectDMURoleNewAccount", title = "-- Selecteer een waarde --"), @ChoiceListValue(value = "PChoice_DMURole.Keuze1", title = "Beïnvloeder"), @ChoiceListValue(value = "PChoice_DMURole.Keuze2", title = "Beoordelaar"), @ChoiceListValue(value = "PChoice_DMURole.Keuze3", title = "Beslisser"), @ChoiceListValue(value = "PChoice_DMURole.Keuze4", title = "Budgethouder"), @ChoiceListValue(value = "PChoice_DMURole.Keuze5", title = "Inkoper"), @ChoiceListValue(value = "PChoice_DMURole.Keuze6", title = "Nvt"), @ChoiceListValue(value = "PChoice_DMURole.Keuze7", title = "Overig") })
	@FindByLabel(label = "DMU")
	public WebElement dMU;
	@TextType()
	@FindByLabel(label = "Initialen")
	public WebElement initialen;
	@ChoiceListType(values = { @ChoiceListValue(value = "Choice_NewSuffix"), @ChoiceListValue(value = "PChoice_Suffix.Keuze1", title = "BA"), @ChoiceListValue(value = "PChoice_Suffix.Keuze2", title = "BSc"), @ChoiceListValue(value = "PChoice_Suffix.Keuze3", title = "jr."), @ChoiceListValue(value = "PChoice_Suffix.Keuze4", title = "MA"), @ChoiceListValue(value = "PChoice_Suffix.Keuze5", title = "MBA"), @ChoiceListValue(value = "PChoice_Suffix.Keuze6", title = "MIM"), @ChoiceListValue(value = "PChoice_Suffix.Keuze7", title = "MSc"), @ChoiceListValue(value = "PChoice_Suffix.Keuze8", title = "RA"), @ChoiceListValue(value = "PChoice_Suffix.Keuze9", title = "RI"), @ChoiceListValue(value = "PChoice_Suffix.Keuze10", title = "sr.") })
	@FindByLabel(label = "Achtertitel")
	public WebElement achtertitel;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@ButtonType()
	@FindByLabel(label = "Beëindigen")
	public WebElement beIndigen;
			
}
