package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page(connection = "CommunityUser", title = "BPG_Invoices")
public class BPG_Invoices {

	@FindBy(xpath = "//select[@class=\"form-control\"][@data-filtername=\"FilterPeriod\"]")
	@ChoiceListType(values = { @ChoiceListValue(value = "Today", title = "Vandaag"), @ChoiceListValue(value = "Yesterday", title = "Gisteren"), @ChoiceListValue(value = "LastWeek", title = "Laatste week"), @ChoiceListValue(value = "LastMonth", title = "Laatste maand"), @ChoiceListValue(value = "LastYear", title = "Laatste jaar") })
	public WebElement period;
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"clear\")]//a")
	public WebElement zoeken;
	@TextType()
	@FindBy(xpath = "//tr[2]//td[6]")
	public WebElement customerInvoiceResults1;
	@TextType()
	@FindBy(xpath = "//*[@class=\"table table-striped table-hover table-shipments fullWidth margin-bottom-none\"]/tbody/tr[3]/td/div/div/div/span/table/tbody/tr[2]/td[3]/span")
	public WebElement Item11;
	@TextType()
	@FindBy(xpath = "//*[@class=\"table table-striped table-hover table-shipments fullWidth margin-bottom-none\"]/tbody/tr[3]/td/div/div/div/span/table/tbody/tr[2]/td[3]/span")
	public WebElement Item12;
	@TextType()
	@FindBy(xpath = "//*[@class=\"table table-striped table-hover table-shipments fullWidth margin-bottom-none\"]/tbody/tr[3]/td/div/div/div/span/table/tbody/tr[4]/td[3]/span")
	public WebElement Item2;
	@TextType()
	@FindBy(xpath = "//*[@class=\"table table-striped table-hover table-shipments fullWidth margin-bottom-none\"]/tbody/tr[3]/td/div/div/div/span/table/tbody/tr[6]/td[3]/span")
	public WebElement Item3;
	


}
