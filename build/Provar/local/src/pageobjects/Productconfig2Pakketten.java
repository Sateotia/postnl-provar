package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="Productconfig2Pakketten"                                
     , summary=""
     , relativeUrl=""
     , connection="Salesforce"
     )             
public class Productconfig2Pakketten {
@FindBy(xpath = "//*[contains(@src,'cscfga__ConfigureProduct')]")
public FrameProductconfiguratie frame_Productconfig1;

	@PageFrame()
	public static class FrameProductconfiguratie {
		
		@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
		@PageWait.Field(timeoutSeconds = 10)
		@LinkType()
		@FindBy(xpath = "//label[text()=\"Prijsgroep\"]/ancestor::td/following-sibling::td[1]//a")
		public WebElement PrijsgroepLink;
		
		@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
		@TextType()
		@PageWait.Field(timeoutSeconds = 10)
		@FindBy(xpath = "//input[contains(@class,\"select2-input\")]")
		public WebElement Prijsgroepinvoer;
		
		@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
		@ButtonType()
		@PageWait.Field(timeoutSeconds = 10)
		@FindBy(xpath = "//div[contains(text(),\"Rate Card Label\")]/ancestor::ul/ancestor::div/ul/li[@class=\"select2-results-dept-0 select2-result select2-result-selectable select2-highlighted\"]")
		public WebElement PGresultaatselect;
		
		@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
		@PageWait.Field(timeoutSeconds = 10)
		@ButtonType()
		@FindBy(xpath = "//div[@class=\"pbHeader\"]//button[text()=\"Next\"]")
		public WebElement NextButton;
		
		@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
		@PageWait.Field(timeoutSeconds = 10)
		@ButtonType()
		@FindBy(xpath = "//div[@class=\"pbHeader\"]//button[text()=\"Finish\"]")
		public WebElement FinishButton;
		
		

	
		
		
	}


	
			
}
