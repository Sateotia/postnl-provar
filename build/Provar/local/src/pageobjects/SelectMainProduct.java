package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="Select Main Product"                                
               , summary=""
               , page="BasketbuilderApp"
               , namespacePrefix="csbb"
               , object="cscfga__Product_Basket__c"
               , connection="Salesforce"
     )             
public class SelectMainProduct {

	@ButtonType()
	@FindBy(xpath = "//div[contains(@class, \"modal-footer\")]//button[contains(@class, \"icon-add\")]")
	public WebElement ADDBASKET;
	@TextType()
	@FindBy(xpath = "//ul[contains(@class, \"jstree-container-ul\")]//ul/li//*[contains(text(),\"Zakelijke Pakketten\")]")
	public WebElement ZakelijkePakketten_Select;
	
}
