package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="SKY_ Acc Address Comp"                                
               , summary=""
               , page="SKY_AccAddressComp"
               , namespacePrefix=""
               , object="Account"
               , connection="Salesforce"
     )             
public class SKY_AccAddressComp {

	@VisualforceBy(componentXPath = "apex:inputfield[@value = \"{!co.FirstName}\"]")
	@SalesforceField(name = "FirstName", object = "Account")
	public WebElement Voornaam;
	@VisualforceBy(componentXPath = "apex:inputfield[@value = \"{!co.Salutation}\"]")
	@SalesforceField(name = "Salutation", object = "Account")
	public WebElement aanhef;
	@VisualforceBy(componentXPath = "apex:inputfield[@value = \"{!co.LastName}\"]")
	@SalesforceField(name = "LastName", object = "Account")
	public WebElement achternaam;
	@VisualforceBy(componentXPath = "apex:inputfield[@value = \"{!ac.Status__pc}\"]")
	@SalesforceField(name = "Status__pc", object = "Account")
	public WebElement status;
	@VisualforceBy(componentXPath = "apex:inputfield[@value = \"{!ac.Postal_Code__c}\"]")
	@SalesforceField(name = "Postal_Code__c", object = "Account")
	public WebElement postcode;
	@VisualforceBy(componentXPath = "apex:inputfield[@value = \"{!ac.House_number__c}\"]")
	@SalesforceField(name = "House_number__c", object = "Account")
	public WebElement huisnummer;
	@VisualforceBy(componentXPath = "apex:inputfield[@value = \"{!ac.Client_Alert__c}\"]")
	@SalesforceField(name = "Client_Alert__c", object = "Account")
	public WebElement klantalert;
	@VisualforceBy(componentXPath = "apex:inputfield[@value = \"{!ac.Facebook_ID__pc}\"]")
	@SalesforceField(name = "Facebook_ID__pc", object = "Account")
	public WebElement facebookID;
	@VisualforceBy(componentXPath = "apex:inputfield[@value = \"{!ac.Twitter_ID__pc}\"]")
	@SalesforceField(name = "Twitter_ID__pc", object = "Account")
	public WebElement twitterID;
	@VisualforceBy(componentXPath = "apex:inputfield[@value = \"{!ac.LinkedIn__pc}\"]")
	@SalesforceField(name = "LinkedIn__pc", object = "Account")
	public WebElement linkedInID;
	@VisualforceBy(componentXPath = "apex:inputfield[@value = \"{!ac.AccountNumber}\"]")
	@SalesforceField(name = "AccountNumber", object = "Account")
	public WebElement accountnummer;
	@ButtonType()
	@VisualforceBy(componentXPath = "apex:commandbutton[@action='{!save}']")
	public WebElement opslaan;
	@BooleanType()
	@VisualforceBy(componentXPath = "apex:selectOption[1]")
	public WebElement consument;
	@ButtonType()
	@VisualforceBy(componentXPath = "apex:commandbutton[@action='{!next}']")
	public WebElement volgende;
	@VisualforceBy(componentXPath = "apex:inputfield[@value = \"{!ac.PersonEmail}\"]")
	@SalesforceField(name = "PersonEmail", object = "Account")
	public WebElement eMail;
	
}
