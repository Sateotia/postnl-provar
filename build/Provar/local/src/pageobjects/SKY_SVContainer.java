package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="SKY_ SV Container"                                
               , summary=""
               , page="SKY_SVContainer"
               , namespacePrefix=""
               , object="Case"
               , connection="SkyTest"
     )             
public class SKY_SVContainer {

	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"requiredInput\")]//input")
	public WebElement ProductCode;
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"requiredInput\")]//textarea")
	public WebElement Product_Omschrijving;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@ButtonType()
	@FindByLabel(label = "Volgende")
	public WebElement volgende;
	@TextType()
	@FindBy(xpath = "//tr[5]/td[2]/div/input")
	public WebElement CB_Code;
	@TextType()
	@FindBy(xpath = "//tr[8]/td[2]/div/input")
	public WebElement Actie_Naam;
	@TextType()
	@FindBy(xpath = "//tr[11]/td[2]/div/input")
	public WebElement Aantal_Stuks;
	@TextType()
	@FindBy(xpath = "//tr[14]/td[2]/div/input")
	public WebElement KortingsTarief;
	@TextType()
	@FindBy(xpath = "//tr[17]/td[2]/div/input")
	public WebElement VerkoopActie;
	@TextType()
	@FindByLabel(label = "Huis-aan-Huis")
	public WebElement ProductKeuze;
	@TextType()
	@FindBy(xpath = "//tr[3]/td[2]/div/span/input")
	public WebElement BeginDatum;
	@TextType()
	@FindBy(xpath = "//tr[6]/td[2]/div/span/input")
	public WebElement Eind_Datum;
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"requiredInput\")]//textarea")
	public WebElement Aanvullende_Informatie;
	
}
