package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="SKY_ Case Send Web Form"                                
               , summary=""
               , page="SKY_CaseSendWebForm"
               , namespacePrefix=""
               , object="Case"
               , connection="Salesforce"
     )             
public class SKY_CaseSendWebForm {

	@BooleanType()
	@FindBy(xpath = "//div[contains(@class, \"interviewFormChoicesWithHelp\")]/div[2]/input")
	public WebElement klantNavraagformulierZending;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@ButtonType()
	@FindByLabel(label = "Volgende")
	public WebElement volgende;
	@BooleanType()
	@FindByLabel(label = "Afzender")
	public WebElement klantNavraagformulierZending1;
	@BooleanType()
	@FindByLabel(label = "Vertraagd")
	public WebElement klantNavraagformulierZending2;
	
}
