package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="MyPageObject"                                
     , connection="SkyTest"
     )             
public class MyPageObject {

	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@LinkType()
	@FindBy(linkText = "Volgende")
	public WebElement volgende1;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.BackgroundActivity(timeoutSeconds = 60)
	@FindBy(css = "#companysection__name")
	@TextType()
	public WebElement bedrijfsnaam;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(field = "Gevonden_bedrijven", timeoutSeconds = 10)
	@FindBy(css = "#companysection__searchoptions")
	@ChoiceListType(values = {
			@ChoiceListValue(value = "85b79300548b2d9433e6af759dcb96ed", title = "Knutsel - 72  Roelantlaan, GELDROP"), @ChoiceListValue(value = "243c7c57a884bffb5698c4de2d7b9bed", title = "Knutsel Frutsel - 21  Hoofddorpweg, AMSTERDAM"), @ChoiceListValue(value = "70870271c949a9a2bf3a7580f44821e0", title = "Knutsel je feestje - 5  Schubertstraat, BOXTEL"), @ChoiceListValue(value = "9c1fcc9def152c23b01b8b6cffeeac9b", title = "Knutsel Klup - 119 C03 Noorderhavenkade, ROTTERDAM"), @ChoiceListValue(value = "615dbcce1b9c800c18daa722a37e3b6b", title = "Knutsel Marijke - 8  Haarlemmermeerlaan, OVERLOON"), @ChoiceListValue(value = "8a9749a8b3dfd3d817c8b31554346749", title = "Knutsel- en Beunhaasproducties - 10  Heselbergherweg, ARNHEM"), @ChoiceListValue(value = "90e3767d837a803683f5bd383d5eae80", title = "Knutselaar.eu - 159  Eyckenstein, AMSTELVEEN"), @ChoiceListValue(value = "c8ccd4f7d77f2fb3ef9ac3a75f4777ea", title = "Knutselatelier Diepenveen - 45  Oranjelaan, DIEPENVEEN"), @ChoiceListValue(value = "2bbbe6cf59b6bff275f82c7a7ba63466", title = "Knutselclub Juffrouw Kikker - 18 1 Fraunhoferstraat, AMSTERDAM"), @ChoiceListValue(value = "aae9405f565f257ab3aa5f23edd9c50a", title = "Knutselcorner - 36  Clarenburg, LEUSDEN"), @ChoiceListValue(value = "484e53a0ef3ee2b52253b5e1c4de4ae4", title = "Knutselen met Roos - 35  Leendert Spaanderlaan, VOLENDAM"), @ChoiceListValue(value = "b64a6a0ad83776df6140cd0f7e693261", title = "Knutselfair - 1281  Kruiszwin, JULIANADORP"), @ChoiceListValue(value = "c9fd69eff9957fcadc7fc7c5ad3f882a", title = "Knutselfleur - 14  Donjon, AMERSFOORT"), @ChoiceListValue(value = "f61039f92a49b0f93030aa40c7e11d36", title = "Knutselgeluk - 98  Ouverturestraat, 'S-GRAVENHAGE"), @ChoiceListValue(value = "9c6780d1ff03420f65fe929b73be6e95", title = "Knutselhoekje.eu - 148  Zwaluw, RIDDERKERK"), @ChoiceListValue(value = "2ee16401a53c17365512413ffca09444", title = "Knutseljuf - 359  Zwolsestraat, 'S-GRAVENHAGE"), @ChoiceListValue(value = "7db9acdd37d13df09d22c4fa989ccc62", title = "Knutselkantine - 23  Orttswarande, BREUKELEN UT"), @ChoiceListValue(value = "7ea2733fa10d3771230e08b3ce004c3b", title = "Knutselkind - 6  Frankenhorst, SASSENHEIM"), @ChoiceListValue(value = "2ce5bd9c312e3667baffb4b6023089f4", title = "Knutselkoffer - 21 A Kerkpad ZZ, SOEST"), @ChoiceListValue(value = "2a5a0fb2831fb856763d64dbdbe13218", title = "Knutselkompanie - 70  Dr. H. Th. s' Jacoblaan, UTRECHT"), @ChoiceListValue(value = "463f5afc8ef4637ff4736dfa73454215", title = "Knutselkookclub - 246  Baron G.A. Tindalstraat, AMSTERDAM"), @ChoiceListValue(value = "86eec9658fbcf77d35546295017a1c03", title = "Knutselkookclub Gooi - 4 wk Schapendrift, LAREN NH"), @ChoiceListValue(value = "c5e16953b626f6ccd419c25042131133", title = "Knutselkraam - 46 C Hoofdstraat, BEST"), @ChoiceListValue(value = "b390bc6623ad5afe24a6d8f975ab16ea", title = "Knutselpakket Compleet - 301  Zuider Kerkedijk, ROTTERDAM"), @ChoiceListValue(value = "d2987d446cda27b757f1d815619ef1d5", title = "Knutselrijk - 58  Grasplant, EINDHOVEN") })
	public WebElement Gevonden_bedrijven;
	@BooleanType()
	@FindByLabel(label = "De heer")
	public WebElement deHeer;
	@FindBy(css = "#contactsection__firstname")
	@TextType()
	public WebElement voornaam1;
	@FindBy(css = "#contactsection__lastname")
	@TextType()
	public WebElement achternaam;
	@FindBy(css = "#contactsection__telephone")
	@TextType()
	public WebElement _0612345678;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@FindBy(css = "#IBANsection a.button.action.buttonCustomStyle")
	@LinkType()
	public WebElement volgende3;
	@FindBy(xpath = "//div[contains(@class, \"first\")]/div[2]/select")
	@ChoiceListType(values = { @ChoiceListValue(value = "A02", title = "Algemene zaken"), @ChoiceListValue(value = "A06", title = "Automatisering/IT"), @ChoiceListValue(value = "A01", title = "Directie"), @ChoiceListValue(value = "A09", title = "E-commerce"), @ChoiceListValue(value = "A13", title = "Facilitaire zaken"), @ChoiceListValue(value = "A05", title = "Financiën"), @ChoiceListValue(value = "A10", title = "Fundraising"), @ChoiceListValue(value = "A12", title = "Inkoop"), @ChoiceListValue(value = "A99", title = "Klantenservice"), @ChoiceListValue(value = "A17", title = "Kwaliteitsbeheer/DZO"), @ChoiceListValue(value = "A14", title = "Logistiek"), @ChoiceListValue(value = "A07", title = "Marketing/Communicatie"), @ChoiceListValue(value = "A99", title = "Onbekend"), @ChoiceListValue(value = "A04", title = "Personeelszaken"), @ChoiceListValue(value = "A03", title = "Postkamer"), @ChoiceListValue(value = "A11", title = "Productie"), @ChoiceListValue(value = "A08", title = "Verkoop") })
	public WebElement contactsection__department1;
	@FindBy(xpath = "//div[contains(@class, \"last\")]/div[2]/select")
	@ChoiceListType(values = { @ChoiceListValue(value = "04", title = "Bedrijfsleider"), @ChoiceListValue(value = "03", title = "Bestuurder"), @ChoiceListValue(value = "02", title = "Directeur"), @ChoiceListValue(value = "01", title = "Eigenaar"), @ChoiceListValue(value = "06", title = "Hoofd/Teamleider"), @ChoiceListValue(value = "05", title = "Manager"), @ChoiceListValue(value = "08", title = "Medewerker"), @ChoiceListValue(value = "99", title = "Onbekend"), @ChoiceListValue(value = "09", title = "Secr./Telef."), @ChoiceListValue(value = "07", title = "Staf") })
	public WebElement contactsection__function1;
	@FindBy(css = "#ibansection__iban")
	@TextType()
	public WebElement nL31ABNA1234567890;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@FindBy(xpath = "//input[contains(@class, \"slds-input\")]")
	@TextType()
	public WebElement _7_a;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@FindBy(xpath = "//*[@name=\"originSelect\"]")
	@ChoiceListType(values = { @ChoiceListValue(value = "Administratief"), @ChoiceListValue(value = "IT Technisch"), @ChoiceListValue(value = "Logistiek"), @ChoiceListValue(value = "Tarief") })
	public WebElement _7_a1;
	@DateType()
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@FindBy(xpath = "//*[@id=\"80:17;a\"]")
	public WebElement _7_a2;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@ChoiceListType(values = { @ChoiceListValue(value = "Choice_NewTitle"), @ChoiceListValue(value = "PChoice_Title.Keuze1", title = "dr."), @ChoiceListValue(value = "PChoice_Title.Keuze2", title = "drs."), @ChoiceListValue(value = "PChoice_Title.Keuze3", title = "prof."), @ChoiceListValue(value = "PChoice_Title.Keuze4", title = "ing."), @ChoiceListValue(value = "PChoice_Title.Keuze5", title = "ir."), @ChoiceListValue(value = "PChoice_Title.Keuze6", title = "mr."), @ChoiceListValue(value = "PChoice_Title.Keuze7", title = "dr. drs."), @ChoiceListValue(value = "PChoice_Title.Keuze8", title = "dr. ir."), @ChoiceListValue(value = "PChoice_Title.Keuze9", title = "drs. ing."), @ChoiceListValue(value = "PChoice_Title.Keuze10", title = "drs. ir."), @ChoiceListValue(value = "PChoice_Title.Keuze11", title = "prof. dr."), @ChoiceListValue(value = "PChoice_Title.Keuze12", title = "prof. dr. ir."), @ChoiceListValue(value = "PChoice_Title.Keuze13", title = "prof. mr."), @ChoiceListValue(value = "PChoice_Title.Keuze14", title = "mr. dr."), @ChoiceListValue(value = "PChoice_Title.Keuze15", title = "mr. drs."), @ChoiceListValue(value = "PChoice_Title.Keuze16", title = "mr. drs. ing."), @ChoiceListValue(value = "PChoice_Title.Keuze17", title = "mr. ing."), @ChoiceListValue(value = "PChoice_Title.Keuze18", title = "mr. ir.") })
	@FindByLabel(label = "Titel")
	public WebElement titel2;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@TextType()
	@FindBy(xpath = "//div[5]//label")
	public WebElement algemenevoorwaarden;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@LinkType()
	@FindBy(linkText = "Bevestig mijn aanmelding")
	public WebElement bevestigMijnAanmelding;
	@FindBy(css = "#emailsection__email")
	@TextType()
	public WebElement infoMailadresCom2;
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"slds-radio_button-group\")]/span[1]/label/span")
	public WebElement AanbiedenviaMijnPost;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@FindBy(css = "div.slds-col--bump-right button.slds-button.slds-button--brand")
	@ButtonType()
	public WebElement volgendeaanbiedstroom1;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(timeoutSeconds = 10)
	@FindBy(css = "button.slds-button.slds-button--brand")
	@ButtonType()
	public WebElement volgendeaanbiedstroom2;
	@TextType()
	@FindBy(xpath = "//h1[contains(@class, \"title\")]")
	public WebElement Bedankt_voor_uw_aanmelding;
	@TextType()
	@FindBy(xpath = "//div[contains(@class, \"slds-size--1-of-1\")]/div[2]/div/ul/li[1]")
	public WebElement Soort;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@LinkType()
	@FindBy(css = "a.slds-navigation-list--vertical__action.slds-text-link--reset")
	public WebElement ClickPrimairContactPersoon;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(field = "zoekContact", timeoutSeconds = 10)
	@TextType()
	@FindBy(css = "input.slds-input.validated.slds-m-top_small")
	public WebElement zoekContact;
	@PageWaitAfter.BackgroundActivity(timeoutSeconds = 60)
	@PageWait.Field(field = "SelectContactOutput", timeoutSeconds = 10)
	@TextType()
	@FindBy(xpath = "//td[1]/div[@class=\"slds-truncate\"]")
	public WebElement SelectContactOutput;
			
}
