package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="Csbb__basketbuilderapp"                                
               , summary=""
               , page="BasketbuilderApp"
               , namespacePrefix="csbb"
               , object="cscfga__Product_Basket__c"
               , connection="SkyTest"
     )             
public class csbb__basketbuilderapp {

	@ButtonType()
	@FindBy(css = "button.btn-new-basket.label.icon.icon-new")
	public WebElement AddProductCPQ;
	
}
