package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="WebpowerForm"                                
     , summary=""
     , relativeUrl=""
     , connection="WebpowerForm"
     )             
public class WebpowerForm {

	@ChoiceListType(values = { @ChoiceListValue(value = "brieven-en-bezorging", title = "Brieven en bezorging"),
			@ChoiceListValue(value = "partijenpost", title = "Partijenpost"),
			@ChoiceListValue(value = "postbus", title = "Postbus"),
			@ChoiceListValue(value = "haalbreng", title = "Breng- en Haalservice"),
			@ChoiceListValue(value = "mijnpost", title = "MijnPost"),
			@ChoiceListValue(value = "digitalepostzegel", title = "Digitale postzegel"),
			@ChoiceListValue(value = "pakketten-of-aangetekend", title = "Pakket of aangetekend zending (met barcode)"),
			@ChoiceListValue(value = "online-verzendservice-of-mijnpakket", title = "Online Verzendservice en MijnPakket"),
			@ChoiceListValue(value = "particulier", title = "Ik ben een consument"),
			@ChoiceListValue(value = "overig", title = "Overig...") })
	@FindBy(css = "#onderwerp")
	public WebElement onderwerp;
	@TextType()
	@FindBy(css = "#message_een")
	public WebElement message;
	@TextType()
	@FindBy(css = "#voornaam")
	public WebElement voornaam;
	@TextType()
	@FindBy(css = "#achternaam")
	public WebElement achternaam;
	@BooleanType()
	@FindBy(css = "#aanhef-0")
	public WebElement dhr;
	@TextType()
	@FindBy(css = "#bedrijfsnaam")
	public WebElement bedrijfsnaam;
	@TextType()
	@FindBy(css = "#postcode")
	public WebElement postcode;
	@TextType()
	@FindBy(css = "#huisnummer")
	public WebElement huisnummer;
	@TextType()
	@FindBy(css = "#straat")
	public WebElement straat;
	@TextType()
	@FindBy(css = "#woonplaats")
	public WebElement plaats;
	@TextType()
	@FindBy(css = "#from_address_een")
	public WebElement eMailadres;
	@TextType()
	@FindBy(css = "#telefoonnummer")
	public WebElement telefoonnummer;
	@ButtonType()
	@FindBy(css = "#ctrl-step-01-fwd input.input")
	public WebElement controlerenEnVerzenden;
	@ButtonType()
	@FindBy(css = "#Send input.input")
	public WebElement versturen;
			
}
