package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="pakketen"                                
     , summary=""
     , relativeUrl=""
     , connection="CommunityUser"
     )             
public class pakketen {

	@LinkType()
	@FindBy(linkText = "Pakketten & Vracht")
	public WebElement pakkettenAndVracht;
	@LinkType()
	@FindBy(linkText = "Versturen")
	public WebElement versturen;
			
}
