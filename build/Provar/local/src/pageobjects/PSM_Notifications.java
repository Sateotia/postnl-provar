package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="PSM_Notifications"                                
     , summary=""
     , relativeUrl=""
     , connection="CommunityUser"
     )             
public class PSM_Notifications {

	@BooleanType()
	@FindBy(xpath = "//input[@name=\"notificationType\"][@id=\"standardNotificationNo\"]")
	public WebElement PlusNotification;
	@FindBy(tagName = "dt")
	@TextType()
	public WebElement Sending_Email;
	@FindBy(linkText = "Nieuwe E-mail Notificatie")
	@LinkType()
	public WebElement nieuweEMailNotificatie1;
	@TextType()
	@FindByLabel(label = "Naam notificatietemplate *")
	public WebElement naamNotificatietemplate;
	@TextType()
	@FindByLabel(label = "E-mailonderwerp *")
	public WebElement eMailonderwerp;
	@LinkType()
	@FindBy(linkText = "Opslaan en verder")
	public WebElement opslaanEnVerder;
			
}
