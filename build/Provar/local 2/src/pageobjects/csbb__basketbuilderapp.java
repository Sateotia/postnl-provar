package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="Csbb__basketbuilderapp"                                
               , page="BasketbuilderApp"
               , object="cscfga__Product_Basket__c"
               , connection="Salesforce", namespacePrefix = "csbb"
     )             
public class csbb__basketbuilderapp {

	@ButtonType()
	@FindBy(css = "button.btn-new-basket.label.icon.icon-new")
	public WebElement AddProductCPQ;
	@ButtonType()
	@FindBy(xpath = "//div[contains(@class, \"modal-footer\")]/div[2]/button")
	public WebElement Add_to_Basket;
	@ButtonType()
	@FindBy(xpath = "//div[contains(@class, \"container\")]/div/header/div[1]/span/button")
	public WebElement CPQ_Icon_Sync;
	@ButtonType()
	@FindBy(xpath = "//div[contains(@class, \"table-cell\")]//button[contains(@class, \"btn-edit\")]")
	public WebElement EditBasket;
	
	
}
//